<?php

namespace app\modules\admin\controllers;

use app\models\GeoCoding;
use app\models\Points;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\components\AccessRule;
use Yii;
use yii\web\Response;

/**
 * Default controller for the `admin` module
 */
class DefaultController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule::className(),
                ],
                'only' => ['index'],
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        if (Yii::$app->request->isPost) {
            $data = Yii::$app->request->post();
            Points::createRoute($data);
            Yii::$app->getSession()->setFlash('success', 'Route successfully saved');
            return $this->render('index');
        } else {
            return $this->render('index');
        }
    }

    public function actionCity()
    {
        $param = json_decode($_POST['param']);
        $result = GeoCoding::reverseGeoCoding($param);
        $response = Yii::$app->response;
        $response->data = $result;
        $response->format = Response::FORMAT_JSON;
    }
}
