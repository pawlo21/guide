<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;


/* @var $this yii\web\View */
/* @var $model app\models\PointsImages */

$this->title = 'Update images';
$this->params['breadcrumbs'][] = $this->title;
$count = 0;
?>

<?php \app\assets\InputAsset::register($this) ?>

<div class="points-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php $form = ActiveForm::begin([
        'options' => [
            'enctype' => 'multipart/form-data'
        ],
    ]); ?>

    <?php if ($model): ?>
    <div id="images">
        <table id="images_table">
            <th> IMAGES <i class="fa fa-plus" id="imageAdd"></i> </th>
    <?php foreach ($model as $image): ?>
        <tr id="image-row-<?= $count?>">
            <td> <div class="fileUpload btn btn-primary">
                    <span id="span-upload">Upload another file</span>
                    <input type="file" class="upload" name="file-<?= $count ?>" id="file-<?= $count ?>" />
                </div></td>
            <td> <?= Html::textInput('img-text-'.$count, $image['title'], ['id' => 'image-text-'.$count, 'placeholder' => 'Title']) ?></td>
            <td> <i class="fa fa-minus" id="image-del-<?= $count ?>" data-value="<?= $count ?>"></i> </td>
        </tr>
        <?php $count++; ?>
    <?php endforeach; ?>
        </table>
    </div>
    <?php else: ?>
        <div id="images">
            <table id="images_table">
                <th> IMAGES <i class="fa fa-plus" id="imageAdd"></i> </th>
                    <tr id="image-row-0">
                        <td> <div class="fileUpload btn btn-primary">
                                <span id="span-upload">Add file</span>
                                <input type="file" class="upload" name="file-new-0" id="file-0" />
                            </div></td>
                        <td> <?= Html::textInput('img-text-0', null, ['id' => 'image-text-0', 'placeholder' => 'Title']) ?></td>
                        <td> <i class="fa fa-minus" id="image-del-0" data-value="0"></i> </td>
                    </tr>
            </table>
        </div>
    <?php endif; ?>


    <div class="form-group" id="submit">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php
$this->registerJs(<<<JS

$("#imageAdd").click(function() {
    itemAdd('image');
});

$(document).delegate( '[id^=image-del]', "click", function() {
    var delete_number = $(this).data("value");
    if (delete_number!=0) {
        $("[id^=image-row-"+delete_number+"]").remove();
        reinit('image', delete_number);
    }
});

function reinit(entity, delete_number) {
    var small;
    var count = $("[id^="+entity+"-del]").length;
     if (entity == 'sound') {
        small = 'snd';
    } else {
        small = 'img';
    }
    for(var i=delete_number; i<=count; i++){
        $("[id^="+entity+"-row-"+i+"]").attr("id", entity+"-row-"+(i-1));
        $("[id^="+small+"-text-"+i+"]").attr("id", entity+"-text-"+(i-1)).attr("name", small+"-text-"+(i-1));
        $("[id^="+entity+"-del-"+i+"]").attr("id", entity+"-del-"+(i-1)).data("value", (i-1));
    }
}

function itemAdd(entity) {
    var small;
    if (entity == 'sound') {
        small = 'snd';
    } else {
        small = 'img';
    }
    var clone = $('[id^='+entity+'-row]:last').clone();
    $("#"+entity+"s_table").append(clone);
    var attrRow=entity+'-row-' + (clone.index() - 1);
    var attrDel=entity+'-del-' + (clone.index() - 1);
    var attrInputId=entity+'-text-' + (clone.index() - 1);
    var attrInputName=small+'-text-' + (clone.index() - 1);
    clone.attr('id', attrRow);
    $('[id^='+entity+'-del]:last').data("value", clone.index()-1).attr('id', attrDel); 
    $('[id^='+entity+'-text]:last').attr('id', attrInputId).attr('name', attrInputName).val(null); 
    $('[id^=points'+entity+']:last').val(null);
    $('[id^=span-upload]').last().text('Add file');
    $('[id^=file-]:last').attr('name', 'file-new-'+(clone.index()-1)).val(null);
}

JS
    , \yii\web\View::POS_READY);
?>
