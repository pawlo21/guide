<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use hosanna\audiojs\AudioJs;

/* @var $this yii\web\View */
/* @var $model app\models\Points */
/* @var $model_sounds app\models\PointsSounds */
/* @var $model_images app\models\PointsImages */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Points', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="points-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Update sounds', ['update-sounds', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Update images', ['update-images', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'description',
            'title',
            'address',
        ],
    ]) ?>

    <?php
    $count = 1;
    foreach ($model_sounds as $sound) {
        echo "<b>" . $count . ". " . $sound['title']."</b>";
        echo AudioJs::widget([
            'files' => 'points/mp3/'.$sound['sound'], //Full URL to Mp3 file here
        ]);
        $count++;
    }
    ?>

    <?php
    $items = [];
    foreach ($model_images as $model_image) {
        $items[] = [
            'url' => $model_image['img_url'],
            'src' => $model_image['thumb_img'],
            'options' => [
                'title' => $model_image['title'],
            ],
        ];
    }?>

    <?= dosamigos\gallery\Gallery::widget(['items' => $items]);?>

</div>
