<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Points */
/* @var $model_images app\models\PointsImages */
/* @var $model_sounds app\models\PointsSounds */

$this->title = 'Create Point';
$this->params['breadcrumbs'][] = ['label' => 'Points', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="points-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'model_images' => $model_images,
        'model_sounds' => $model_sounds,
    ]) ?>

</div>
