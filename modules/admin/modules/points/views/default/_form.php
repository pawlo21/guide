<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Points */
/* @var $model_images app\models\PointsImages */
/* @var $model_sounds app\models\PointsSounds */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="points-form">

    <?php \app\assets\PointsAsset::register($this) ?>

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'city_id')->dropDownList(
        ArrayHelper::map(\app\models\Cities::find()->all(), 'id', 'title'), ['prompt' => 'Select city']
    ) ?>

    <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>

    <p class="hider"><b>Map</b></p>
    <div id="hidden" style="display: none;">
        <div id="map"></div>
    </div>

    <?php if ($model->isNewRecord) : ?>
    <div id="images">
        <table id="images_table">
            <th> IMAGES <i class="fa fa-plus" id="imageAdd"></i> </th>
            <tr id="image-row-0">
                <td> <?= $form->field($model_images, 'img[]')->fileInput(['multiple' => false, 'accept' => 'image/*']) ?> </td>
                <td> <?= Html::textInput('img-text-0', null, ['id' => 'image-text-0', 'placeholder' => 'Title']) ?></td>
                <td> <i class="fa fa-minus" id="image-del-0" data-value="0"></i> </td>
            </tr>
        </table>
    </div>

    <div id="sounds">
        <table id="sounds_table">
            <th> SOUNDS <i class="fa fa-plus" id="soundAdd"></i> </th>
            <tr id="sound-row-0">
                <td> <?= $form->field($model_sounds, 'sound[]')->fileInput(['multiple' => false, 'accept' => 'sound/*']) ?> </td>
                <td> <?= Html::textInput('snd-text-0', null, ['id' => 'sound-text-0', 'placeholder' => 'Title']) ?></td>
                <td> <i class="fa fa-minus" id="sound-del-0" data-value="0"></i> </td>
            </tr>
        </table>
    </div>
    <?php endif ?>

    <?= Html::hiddenInput('newRecord', $model->isNewRecord ? 1 : 0, ['id' => 'newRecord']) ?>

    <?= Html::hiddenInput('curr_lat', 0, ['id' => 'lat']) ?>

    <?= Html::hiddenInput('curr_lng', 0, ['id' => 'lng']) ?>

    <div class="form-group" id="submit">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <script src="https://maps.googleapis.com/maps/api/js?key=<?= Yii::$app->params['google_api_key'] ?>&callback=initMap"></script>
    <?php ActiveForm::end(); ?>

</div>

<?php
$this->registerJs(<<<JS

var newRecord = $("#newRecord").val();
var el = $("#points-address").val();

$(document).ready(function(){
    $(".hider").click(function(){
        $("#hidden").slideToggle("slow");
        if (newRecord == 0) {
            $.ajax({
                type:'POST',
                url:'scale',
                dataType:'json',
                data:"address="+el,
                success:function(data) {
                    center = {lat: data['lat'], lng: data['lng']};
                    zoom = 14;
                    initMap();
                }
            });
        } else {
            initMap();
            return false;
        }
    });
});

$( "#points-city_id" ).change(function() {
     var city = $("#points-city_id" ).val();
     $.ajax({
        type:'POST',
        url:'scale',
        dataType:'json',
        data:"city="+city,
        success:function(data) {
            center = {lat: data['lat'], lng: data['lng']};
            zoom = 14;
            initMap();
        }
     });
});

$("#imageAdd").click(function() {
    itemAdd('image');
});

$("#soundAdd").click(function() {
    itemAdd('sound');
});

$(document).delegate( '[id^=image-del]', "click", function() {
    var delete_number = $(this).data("value");
    if (delete_number!=0) {
        $("[id^=image-row-"+delete_number+"]").remove();
        reinit('image', delete_number);
    }
});

$(document).delegate( '[id^=sound-del]', "click", function() {
    var delete_number = $(this).data("value");
    if (delete_number!=0) {
        $("[id^=sound-row-"+delete_number+"]").remove();
        reinit('sound', delete_number);
    }
});

function reinit(entity, delete_number) {
    var small;
    var count = $("[id^="+entity+"-del]").length;
     if (entity == 'sound') {
        small = 'snd';
    } else {
        small = 'img';
    }
    for(var i=delete_number; i<=count; i++){
        $("[id^="+entity+"-row-"+i+"]").attr("id", entity+"-row-"+(i-1));
        $("[id^="+small+"-text-"+i+"]").attr("id", entity+"-text-"+(i-1)).attr("name", small+"-text-"+(i-1));
        $("[id^="+entity+"-del-"+i+"]").attr("id", entity+"-del-"+(i-1)).data("value", (i-1));
    }
}

function itemAdd(entity) {
    var small;
    if (entity == 'sound') {
        small = 'snd';
    } else {
        small = 'img';
    }
    var clone = $('[id^='+entity+'-row]:last').clone();
    $("#"+entity+"s_table").append(clone);
    var attrRow=entity+'-row-' + (clone.index() - 1);
    var attrDel=entity+'-del-' + (clone.index() - 1);
    var attrInputId=entity+'-text-' + (clone.index() - 1);
    var attrInputName=small+'-text-' + (clone.index() - 1);
    clone.attr('id', attrRow);
    $('[id^='+entity+'-del]:last').data("value", clone.index()-1).attr('id', attrDel); 
    $('[id^='+entity+'-text]:last').attr('id', attrInputId).attr('name', attrInputName).val(null); 
    $('[id^=points'+entity+']:last').val(null);
}

JS
    , \yii\web\View::POS_READY);

$this->registerJs(<<<JS

    var map;
    var center = {lat: 37.769, lng: -122.446};
    var zoom = 1;
    var marker;
    var infowindow;
    var lat;
    var lng;
        
    function initMap() {
        map = new google.maps.Map(document.getElementById('map'), {
            zoom: zoom,
            center: center,
            mapTypeId: 'terrain'
        });
      
        // This event listener will call addMarker() when the map is clicked.
        map.addListener('click', function(event) {
            if(marker!=undefined) {
                marker.setMap(null);
            }  
            addMarker(event.latLng);
            getAddress();
            marker.setMap(map);
            infowindow.open(map, marker);
        });

    }

    // Adds a marker to the map and push to the array.
    function addMarker(location) {
        marker = new google.maps.Marker({
            position: location,
            map: map,
            draggable: true
        });

        lat = marker.getPosition().lat().toFixed(6);
        lng = marker.getPosition().lng().toFixed(6)
        $("#lat").val(lat);
        $("#lng").val(lng);        
        infowindow = new google.maps.InfoWindow({
            content: lat + ',' + lng
        });
    }
    
    function getAddress() {
        var data = {'lat': lat, 'lng': lng};
        $.ajax({
            type:'POST',
            url:'city',
            dataType:'json',
            data: data, 
            success:function(data) {
                $("#points-address").val(data);
            }
        });
    }

JS
    , \yii\web\View::POS_HEAD);

?>
