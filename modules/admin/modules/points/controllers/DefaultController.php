<?php

namespace app\modules\admin\modules\points\controllers;

use app\models\Cities;
use app\models\GeoCoding;
use app\models\PointsImages;
use app\models\PointsSounds;
use Yii;
use app\models\Points;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\web\Response;
use yii\imagine\Image;
use Imagine\Image\Box;
use yii\helpers\VarDumper;

/**
 * DefaultController implements the CRUD actions for Points model.
 */
class DefaultController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Points models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Points::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Points model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $model_sounds = PointsSounds::getSoundsByPointId($id);
        $model_images = PointsImages::getImagesByPointId($id);
        return $this->render('view', [
            'model' => $model,
            'model_sounds' => $model_sounds,
            'model_images' => $model_images,
        ]);
    }

    /**
     * Creates a new Points model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Points();
        $model_images = new PointsImages();
        $model_sounds = new PointsSounds();

        if ($model->load(Yii::$app->request->post()) && $model_images->load(Yii::$app->request->post())) {

            $params = Yii::$app->request->post();
            $address = GeoCoding::reverseGeoCoding($model->address);
            $model->lat = (string)$params['curr_lat'];
            $model->lng = (string)$params['curr_lng'];
            $model->save();

            $mp3 = UploadedFile::getInstances($model_sounds, 'sound');
            if ($mp3) {
                $count = 0;
                foreach ($mp3 as $sound) {
                    $model_sound = new PointsSounds();
                    $model_sound->title = $params['snd-text-' . $count] != '' ? $params['snd-text-' . $count] : null;
                    $model_sound->point_id = $model->id;
                    $unique_name = uniqid('mp3_');
                    $full_path = 'uploads/points/mp3/' . $unique_name . '.' . $sound->extension;
                    $sound->saveAs($full_path);
                    $model_sound->sound = $unique_name . '.' . $sound->extension;
                    $model_sound->save();
                    $count++;
                }
            }

            $images = UploadedFile::getInstances($model_images, 'img');
            if ($images) {
                $count = 0;
                foreach ($images as $image) {
                    $model_image = new PointsImages();
                    $model_image->title = $params['img-text-' . $count] != '' ? $params['img-text-' . $count] : null;
                    $model_image->point_id = $model->id;
                    $unique_name = uniqid('pnt_');
                    $full_path = 'uploads/points/imgs/' . $unique_name . '.' . $image->extension;
                    $full_path_thumb = 'uploads/points/imgs/thumb/' . $unique_name . '.' . $image->extension;
                    $image->saveAs($full_path);
                    Image::getImagine()
                        ->open($full_path)
                        ->resize(new Box(100, 100))
                        ->save($full_path_thumb, ['quality' => 100]);
                    $model_image->img = $unique_name . '.' . $image->extension;
                    $model_image->save();
                    $count++;
                }
            }
            return $this->redirect(['index', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
            'model_images' => $model_images,
            'model_sounds' => $model_sounds,
        ]);
    }

    /**
     * Updates an existing Points model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionUpdateImages($id)
    {
        $model = PointsImages::getImagesByPointId($id);

        if (Yii::$app->request->post()) {
            $params = Yii::$app->request->post();

            for ($i = 0; $i < sizeof($model);$i++) {
                if (isset($_FILES['file-'.$i])) {
                    if ($_FILES['file-'.$i]['name'] != '') {
                        $unique_name = uniqid('pnt_');
                        $full_path = 'uploads/points/imgs/' . $unique_name . '.jpg';
                        $full_path_thumb = 'uploads/points/imgs/thumb/' . $unique_name . '.jpg';
                        file_put_contents($full_path, file_get_contents($_FILES['file-'.$i]['tmp_name']));
                        Image::getImagine()
                            ->open($full_path)
                            ->resize(new Box(100, 100))
                            ->save($full_path_thumb, ['quality' => 100]);
                        $file = PointsImages::findOne([$model[$i]['id']]);
                        unlink('uploads/points/imgs/'.$file->img);
                        unlink('uploads/points/imgs/thumb/'.$file->img);
                        $file->img = $unique_name . '.jpg';
                        $file->update();
                    }
                } else {
                    $file = PointsImages::findOne([$model[$i]['id']]);
                    unlink('uploads/points/imgs/'.$file->img);
                    unlink('uploads/points/imgs/thumb/'.$file->img);
                    $file->delete();
                }

            }
            $imgs_size = sizeof($_FILES);
            for ($i = 0; $i<=$imgs_size; $i++){
                if (isset($_FILES['file-new-'.$i])){
                    if ($_FILES['file-new-'.$i]['name'] != '') {
                        $unique_name = uniqid('pnt_');
                        $full_path = 'uploads/points/imgs/' . $unique_name . '.jpg';
                        $full_path_thumb = 'uploads/points/imgs/thumb/' . $unique_name . '.jpg';
                        file_put_contents($full_path, file_get_contents($_FILES['file-new-' . $i]['tmp_name']));
                        Image::getImagine()
                            ->open($full_path)
                            ->resize(new Box(100, 100))
                            ->save($full_path_thumb, ['quality' => 100]);
                        $model_image = new PointsImages();
                        $model_image->img = $unique_name . '.jpg';
                        $model_image->point_id = $id;
                        $model_image->title = $params['img-text-'.$i];
                        $model_image->save();
                    }
                }
            }
            $model = PointsImages::getImagesByPointId($id);

            for ($i=0; $i<sizeof($model); $i++){
                $entity = PointsImages::findOne([$model[$i]['id']]);
                $entity->title = $params['img-text-'.$i];
                $entity->update();
            }

            return $this->redirect(['view', 'id' => $id]);
        }

        return $this->render('updateImages', [
            'model' => $model,
        ]);
    }

    public function actionUpdateSounds($id)
    {
        $model = PointsSounds::getSoundsByPointId($id);

        if (Yii::$app->request->post()) {
            $params = Yii::$app->request->post();

            for ($i = 0; $i < sizeof($model);$i++) {
                if (isset($_FILES['file-'.$i])) {
                    if ($_FILES['file-'.$i]['name'] != '') {
                        $unique_name = uniqid('mp3_');
                        $full_path = 'uploads/points/mp3/' . $unique_name . '.mp3';
                        file_put_contents($full_path, file_get_contents($_FILES['file-'.$i]['tmp_name']));
                        $file = PointsSounds::findOne([$model[$i]['id']]);
                        unlink('uploads/points/mp3/'.$file->sound);
                        $file->sound = $unique_name . '.mp3';
                        $file->update();
                    }
                } else {
                    $file = PointsSounds::findOne([$model[$i]['id']]);
                    unlink('uploads/points/mp3/'.$file->sound);
                    $file->delete();
                }
            }

            $sounds_size = sizeof($_FILES);
            for ($i = 0; $i<=$sounds_size; $i++){
                if (isset($_FILES['file-new-'.$i])){
                    if ($_FILES['file-new-'.$i]['name'] != '') {
                        $unique_name = uniqid('mp3_');
                        $full_path = 'uploads/points/mp3/' . $unique_name . '.mp3';
                        file_put_contents($full_path, file_get_contents($_FILES['file-new-' . $i]['tmp_name']));
                        $model_sound = new PointsSounds();
                        $model_sound->sound = $unique_name . '.mp3';
                        $model_sound->point_id = $id;
                        $model_sound->title = $params['snd-text-'.$i];
                        $model_sound->save();
                    }
                }
            }

            $model = PointsSounds::getSoundsByPointId($id);

            for ($i=0; $i<sizeof($model); $i++){
                $entity = PointsSounds::findOne([$model[$i]['id']]);
                $entity->title = $params['snd-text-'.$i];
                $entity->update();
            }
            return $this->redirect(['view', 'id' => $id]);
        }

        return $this->render('updateSounds', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Points model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Points model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Points the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Points::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionCity()
    {
        $params = Yii::$app->request->post();
        $address = GeoCoding::geoCoding($params['lat'], $params['lng']);

        $response = Yii::$app->response;
        $response->data = $address;
        $response->format = Response::FORMAT_JSON;
    }

    public function actionScale()
    {
        $params = Yii::$app->request->post();
        if (isset($params['city'])) {
            $city = Cities::findOne((int)$params['city'])->title;
            $address = GeoCoding::reverseGeoCoding($city);
        } else {
            $city = $params['address'];
            $address = GeoCoding::reverseGeoCoding($city);
        }
        $response = Yii::$app->response;
        $response->data = $address;
        $response->format = Response::FORMAT_JSON;
    }
}
