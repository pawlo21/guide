<?php

namespace app\modules\admin\modules\routes\controllers;

use app\models\Destinations;
use app\models\Points;
use Yii;
use app\models\Routes;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * DefaultController implements the CRUD actions for Routes model.
 */
class DefaultController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Routes models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Routes::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Routes model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = Routes::getRouteById($id);

//        VarDumper::dump($model, 10, true); exit;

        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new Routes model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $modelRoute = new Routes();
        if ($modelRoute->load(Yii::$app->request->post())) {
            $param = Yii::$app->request->post();
            $route = new Routes();
            $route->title = $param['Routes']['title'];
            $route->city_id = $param['Routes']['city_id'];
            $route->save();

            foreach ($param['Destinations'] as $item) {
                $destination = new Destinations();
                $destination->point_id = $item['point_id'];
                $destination->route_id = $route->id;
                $destination->save();
            }
            return $this->redirect(['index']);

        }

        return $this->render('create', [
            'modelRoute' => $modelRoute,
            'modelsDestination' =>[new Destinations()],
        ]);
    }

    /**
     * Updates an existing Routes model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $routes = $this->findPoints($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $params = Yii::$app->request->post();
            $route = Routes::getRouteByTitleAndCity($params['Routes']['title'], $params['Routes']['city_id']);
            Destinations::deleteAll('route_id = :route_id ', [':route_id' => $route['id']]);
            foreach ($params['Points'] as $item) {
                $destination = new Destinations();
                $destination->point_id = $item['id'];
                $destination->route_id = $route['id'];
                $destination->save();
            }
            return $this->redirect(['index', 'id' => $model->id]);
        }

        return $this->render('update', [
            'modelRoute' => $model,
            'modelsDestination' => $routes
        ]);
    }

    /**
     * Deletes an existing Routes model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Routes model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Routes the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Routes::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    protected function findPoints($id)
    {
        $routes = Destinations::find()
            ->where(['route_id' => $id])
            ->asArray()
            ->all();

        foreach ($routes as $route) {
            $points[] = Points::findOne($route['point_id']);
        }

        return $points;
    }

    public function actionPoints()
    {
        $param = json_decode($_POST['param']);
        $result = Points::getPoints($param);
        $response = Yii::$app->response;
        $response->data = $result;
        $response->format = Response::FORMAT_JSON;
    }
}
