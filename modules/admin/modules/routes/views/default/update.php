<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $modelRoute app\models\Routes */
/* @var $modelsDestination app\models\Destinations */

$this->title = 'Update Routes: ' . $modelRoute->title;
$this->params['breadcrumbs'][] = ['label' => 'Routes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $modelRoute->title, 'url' => ['view', 'id' => $modelRoute->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="routes-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'modelRoute' => $modelRoute,
        'modelsDestination' => $modelsDestination
    ]) ?>

</div>
