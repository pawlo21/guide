<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $modelRoute app\models\Routes */
/* @var $modelsDestination app\models\Destinations */

$this->title = 'Create Route';
$this->params['breadcrumbs'][] = ['label' => 'Routes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="routes-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'modelRoute' => $modelRoute,
        'modelsDestination' => $modelsDestination
    ]) ?>

</div>
