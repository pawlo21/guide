<?php
/* @var $modelRoute app\models\Routes */
/* @var $modelsDestination app\models\Destinations */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use wbraganca\dynamicform\DynamicFormWidget;
use yii\helpers\ArrayHelper;
?>

<div class="customer-form">

    <?php $form = ActiveForm::begin(['id' => 'dynamic-form']); ?>
    <div class="row">
        <div class="col-sm-6">
            <?= $form->field($modelRoute, 'title')->textInput(['maxlength' => true]) ?>

            <?=
            $form->field($modelRoute, 'city_id')->dropDownList(
                ArrayHelper::map(\app\models\Cities::find()->all(), 'id', 'title'), ['prompt' => 'Select city']
            )
            ?>

        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-body">
            <?php DynamicFormWidget::begin([
                'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                'widgetBody' => '.container-items', // required: css class selector
                'widgetItem' => '.item', // required: css class
//                'limit' => 4, // the maximum times, an element can be cloned (default 999)
                'min' => 1, // 0 or 1 (default 1)
                'insertButton' => '.add-item', // css class
                'deleteButton' => '.remove-item', // css class
                'model' => $modelsDestination[0],
                'formId' => 'dynamic-form',
                'formFields' => [
                    'postal_code',
                ],
            ]); ?>

            <div class="container-items"><!-- widgetContainer -->
                <?php foreach ($modelsDestination as $i => $modelDestination): ?>
                    <div class="item panel panel-default"><!-- widgetBody -->
                        <div class="panel-heading">
                            <h3 class="panel-title pull-left">Point</h3>
                            <div class="pull-right">
                                <button type="button" class="add-item btn btn-success btn-xs"><i class="glyphicon glyphicon-plus"></i></button>
                                <button type="button" class="remove-item btn btn-danger btn-xs"><i class="glyphicon glyphicon-minus"></i></button>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-body">
                            <?php
                            // necessary for update action.
                            if (! $modelDestination->isNewRecord) {
                                echo Html::activeHiddenInput($modelDestination, "[{$i}]id");
                            }
//                            VarDumper::dump($modelsDestination, 10, true); exit;
                            ?>
                            <?=
                            $form->field($modelDestination, $modelRoute->isNewRecord ? "[{$i}]point_id" : "[{$i}]id")->dropDownList(
                                ArrayHelper::map(\app\models\Points::find()->all(), 'id','title'), ['prompt' => 'Select point']
                            )
//                            ?>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
            <?php DynamicFormWidget::end(); ?>
        </div>
    </div>

        <div class="form-group">
            <?= Html::submitButton($modelRoute->isNewRecord ? 'Create' : 'Update', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>
    <?php
    $this->registerJs(<<<JS

$(".dynamicform_wrapper").on("beforeInsert", function(e, item) {
    console.log("beforeInsert");
});

$(".dynamicform_wrapper").on("afterInsert", function(e, item) {
    var count = $(":input[id^=destinations-]").length-1;
    
    var first = document.getElementById('destinations-0-point_id');
    var options = first.innerHTML;

    var second = document.getElementById('destinations-'+count+'-point_id');
    second.innerHTML = options;
    
    console.log("afterInsert");
});

$(".dynamicform_wrapper").on("beforeDelete", function(e, item) {
    if (! confirm("Are you sure you want to delete this item?")) {
        return false;
    }
    return true;
});

$(".dynamicform_wrapper").on("afterDelete", function(e) {
    console.log("Deleted item!");
});

$(".dynamicform_wrapper").on("limitReached", function(e, item) {
    alert("Limit reached");
});

$( "#routes-city_id" ).change(function() {
    var val = $("#routes-city_id").val();
    var el;
     $.ajax({
        type:'POST',
        url:'points',
        dataType:'json',
        data:"param="+JSON.stringify(val),
        success:function(data) {

            var count = $(":input[id^=destinations-]").length-1;

            for (var i=0;i<=count;i++) {
                el = $("#destinations-"+i+"-point_id");
                el.empty(); // remove old options
                $.each(data, function(key,value) {
                    el.append($("<option></option>")
                    .attr("value", value.id).text(value.title));
                });
            }
        }
     });

});

JS
        , \yii\web\View::POS_READY);

    ?>
