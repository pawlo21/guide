<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Routes */

$this->title = $model['title'];
$this->params['breadcrumbs'][] = ['label' => 'Routes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$i = 0;
?>
<div class="routes-view">
    <?php \app\assets\CustomAsset::register($this) ?>

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model['id']], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model['id']], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>


    <table cellspacing="0">
        <tr>
            <td>ID</td>
            <td><?= $model['id'] ?></td>
        </tr>
        <tr>
            <td>Title</td>
            <td><?= $model['title'] ?></td>
        </tr>
        <tr>
            <td>City</td>
            <td><?= $model['city'] ?></td>
        </tr>
        <tr>
            <th class="hider" id="points_show">Points +</th>
            <td></td>
        </tr>
        <?php foreach ($model['points'] as $point): ?>
            <?php $i++?>
            <tr class="hidden toogle">
                <th><?= $i?></th>
            </tr>
            <tr class="hidden toogle">
                <td>Coordinates</td>
                <td><?= $point['lat'] . ' ' . $point['lng']?></td>
            </tr>
            <tr class="hidden toogle">
                <td>Title</td>
                <td><?= $point['title'] ?></td>
            </tr>
            <tr class="hidden toogle">
                <td>Description</td>
                <td><?= $point['description'] ?></td>
            </tr>
        <?php endforeach; ?>
    </table>
</div>

<?php
$this->registerJs(<<<JS

var toogle = $(".toogle");
$(document).ready(function(){
    $(".hider").click(function(){
        if ( toogle.hasClass("hidden") ) {
            toogle.removeClass("hidden");
        } else {
            toogle.addClass("hidden");
        }
        return false;
    });
});   

JS
, \yii\web\View::POS_READY);

?>
