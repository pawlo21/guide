<?php
/**
 * Created by PhpStorm.
 * User: developer
 * Date: 10/10/17
 * Time: 1:39 PM
 */

namespace app\components;

class AccessRule extends \yii\filters\AccessRule
{
    /**
     * @inheritdoc
     */
    protected function matchRole($user)
    {
//        var_dump($user);`
        if (empty($this->roles)) {
            return true;
        }
        foreach ($this->roles as $role) {
            if ($role === '?') {
                if ($user->getIsGuest()) {
                    return true;
                }
            } elseif ($role === '@') {
                if (!$user->getIsGuest()) {
                    return true;
                }
            } elseif ($user->can($role)) {
                return true;
            } elseif ($identity = $user->getIdentity()) {
                if ($identity->can($role)) {
                    return true;
                }
            }
        }
        return false;
    }
}