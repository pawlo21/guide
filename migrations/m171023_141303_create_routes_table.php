<?php

use yii\db\Migration;

/**
 * Handles the creation of table `routes`.
 */
class m171023_141303_create_routes_table extends Migration
{

    const ROUTES_TABLE = 'routes';
    const CITIES_TABLE = 'cities';
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable(self::ROUTES_TABLE, [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'city_id' => $this->integer(),
        ]);

        $this->addForeignKey(
            'fk-routes-city_id',
            self::ROUTES_TABLE,
            'city_id',
            self::CITIES_TABLE,
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk-routes-city_id', self::ROUTES_TABLE);
        $this->dropTable(self::ROUTES_TABLE);
    }
}
