<?php

use yii\db\Migration;

/**
 * Handles the creation of table `cities`.
 */
class m171023_141123_create_cities_table extends Migration
{

    const CITIES_TABLE = 'cities';
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable(self::CITIES_TABLE, [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'logo' => $this->string()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable(self::CITIES_TABLE);
    }
}
