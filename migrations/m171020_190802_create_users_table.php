<?php

use yii\db\Migration;

/**
 * Handles the creation of table `users`.
 */
class m171020_190802_create_users_table extends Migration
{
    const USERS_TABLE = 'users';
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable(self::USERS_TABLE, [
            'id' => $this->primaryKey(),
            'login' => $this->string(),
            'accessToken' => $this->string(),
            'password' => $this->string(),
            'authKey' => $this->string(),
        ]);

        $access_token = Yii::$app->getSecurity()->generateRandomString();
        $password = Yii::$app->getSecurity()->generatePasswordHash('admin');

        $this->insert(self::USERS_TABLE,[
            'login' => 'admin',
            'accessToken' => $access_token,
            'password' => $password,
            'authKey' => $access_token,
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->delete(self::USERS_TABLE, ['id' => 1]);
        $this->dropTable(self::USERS_TABLE);
    }
}
