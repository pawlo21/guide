<?php

use yii\db\Migration;

/**
 * Handles the creation of table `points_sounds`.
 */
class m171025_122143_create_points_sounds_table extends Migration
{

    const POINTS_SOUNDS_TABLE  = 'points_sounds';
    const POINTS_TABLE = 'points';
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable(self::POINTS_SOUNDS_TABLE, [
            'id' => $this->primaryKey(),
            'point_id' => $this->integer(),
            'sound' => $this->string()
        ]);

        $this->addForeignKey(
            'fk-sounds-point_id',
            self::POINTS_SOUNDS_TABLE,
            'point_id',
            self::POINTS_TABLE,
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk-sounds-point_id', self::POINTS_SOUNDS_TABLE);
        $this->dropTable(self::POINTS_SOUNDS_TABLE);
    }
}
