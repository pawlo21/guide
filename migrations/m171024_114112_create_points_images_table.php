<?php

use yii\db\Migration;

/**
 * Handles the creation of table `points_images`.
 */
class m171024_114112_create_points_images_table extends Migration
{

    const POINTS_IMAGES_TABLE  = 'points_images';
    const POINTS_TABLE = 'points';
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable(self::POINTS_IMAGES_TABLE, [
            'id' => $this->primaryKey(),
            'point_id' => $this->integer(),
            'img' => $this->string()
        ]);

        $this->addForeignKey(
            'fk-images-point_id',
            self::POINTS_IMAGES_TABLE,
            'point_id',
            self::POINTS_TABLE,
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk-images-point_id', self::POINTS_IMAGES_TABLE);
        $this->dropTable(self::POINTS_IMAGES_TABLE);
    }
}
