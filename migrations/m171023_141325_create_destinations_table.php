<?php

use yii\db\Migration;

/**
 * Handles the creation of table `destinations`.
 */
class m171023_141325_create_destinations_table extends Migration
{

    const DESTINATIONS_TABLE = 'destinations';
    const ROUTES_TABLE = 'routes';
    const POINTS_TABLE = 'points';
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable(self::DESTINATIONS_TABLE, [
            'id' => $this->primaryKey(),
            'route_id' => $this->integer(),
            'point_id' => $this->integer(),
        ]);

        $this->addForeignKey(
            'fk-destinations-route_id',
            self::DESTINATIONS_TABLE,
            'route_id',
            self::ROUTES_TABLE,
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-destinations-point_id',
            self::DESTINATIONS_TABLE,
            'point_id',
            self::POINTS_TABLE,
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk-destinations-route_id', self::DESTINATIONS_TABLE);
        $this->dropForeignKey('fk-destinations-point_id', self::DESTINATIONS_TABLE);
        $this->dropTable(self::DESTINATIONS_TABLE);
    }
}
