<?php

use yii\db\Migration;

/**
 * Class m171103_104258_add_title_field_to_images_and_sounds
 */
class m171103_104258_add_title_field_to_images_and_sounds extends Migration
{
    const POINTS_IMAGES_TABLE  = 'points_images';
    const POINTS_SOUNDS_TABLE  = 'points_sounds';

    public function up()
    {
        $this->addColumn(self::POINTS_SOUNDS_TABLE, 'title', $this->string());
        $this->addColumn(self::POINTS_IMAGES_TABLE, 'title', $this->string());
    }

    public function down()
    {
        $this->dropColumn(self::POINTS_IMAGES_TABLE, 'title');
        $this->dropColumn(self::POINTS_SOUNDS_TABLE, 'title');
    }
}
