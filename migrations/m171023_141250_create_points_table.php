<?php

use yii\db\Migration;

/**
 * Handles the creation of table `points`.
 */
class m171023_141250_create_points_table extends Migration
{

    const POINTS_TABLE = 'points';
    const CITIES_TABLE = 'cities';
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable(self::POINTS_TABLE, [
            'id' => $this->primaryKey(),
            'city_id' => $this->integer(),
            'lat' => $this->string(),
            'lng' => $this->string(),
            'description' => $this->string(1000),
            'title' => $this->string(),
            'address' => $this->string(),
        ]);

        $this->addForeignKey(
            'fk-points-city_id',
            self::POINTS_TABLE,
            'city_id',
            self::CITIES_TABLE,
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk-points-city_id', self::POINTS_TABLE);
        $this->dropTable(self::POINTS_TABLE);
    }
}
