<?php

namespace app\controllers;

use app\models\Cities;
use app\models\Points;
use yii\filters\ContentNegotiator;
use yii\filters\Cors;
use yii\web\Response;
use yii\rest\ActiveController;
use Yii;

class PointsController extends ActiveController
{
    public $modelClass = 'app\models\Points';

    /**
     * @return array
     * behaviors initialize
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        //Предоставляем возможность пользоваться нашим АPI посредством ajax-запросов с других доменов
        $behaviors['corsFilter'] = [
            'class' => Cors::className(),
        ];

        //Отдача ответов в фомате JSON
        $behaviors['contentNegotiator'] = [
            'class' => ContentNegotiator::className(),
            'formats' => [
                'application/json' => Response::FORMAT_JSON,
            ]
        ];

        return $behaviors;
    }

    /**
     * @return array
     * redefine standard actions
     */
    public function actions()
    {
        $actions = parent::actions();
        unset($actions['index']);
        return $actions;
    }

    /**
     * function for getting routes
     */
    public function actionIndex()
    {
        $response = Yii::$app->response;
        $params = Yii::$app->request->get();

        $points_list = Points::getPoints($params['id']);

        $response->data = $points_list;
        $response->format = Response::FORMAT_JSON;
    }

    public function actionPoint()
    {
        $response = Yii::$app->response;
        $params = Yii::$app->request->get();

        $point = Points::getPoint($params['id']);

        $response->data = $point;
        $response->format = Response::FORMAT_JSON;
    }
}
