<?php

namespace app\controllers;

use app\models\PairsImages;
use app\models\Routes;
use yii\filters\ContentNegotiator;
use yii\filters\Cors;
use yii\web\Response;
use yii\rest\ActiveController;
use Yii;
use yii\filters\auth\HttpBearerAuth;

class RoutesController extends ActiveController
{
    public $modelClass = 'app\models\Routes';

    /**
     * @return array
     * behaviors initialize
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        //Предоставляем возможность пользоваться нашим АPI посредством ajax-запросов с других доменов
        $behaviors['corsFilter'] = [
            'class' => Cors::className(),
        ];

        //Отдача ответов в фомате JSON
        $behaviors['contentNegotiator'] = [
            'class' => ContentNegotiator::className(),
            'formats' => [
                'application/json' => Response::FORMAT_JSON,
            ]
        ];

        return $behaviors;
    }

    /**
     * @return array
     * redefine standard actions
     */
    public function actions()
    {
        $actions = parent::actions();
        unset($actions['index']);
        return $actions;
    }

    /**
     * function for getting routes
     */
    public function actionIndex()
    {
        $response = Yii::$app->response;
        $params = Yii::$app->request->get();
        $routes = Routes::getRoutes($params['id']);

        $response->data = $routes;
        $response->format = Response::FORMAT_JSON;
    }

    public function actionRoute()
    {
        $response = Yii::$app->response;
        $params = Yii::$app->request->get();
        $routes = Routes::getRouteById($params['id']);

        $response->data = $routes;
        $response->format = Response::FORMAT_JSON;
    }

//    public function actionCity()
//    {
//        $params = Yii::$app->request->get();
//        $response = Yii::$app->response;
//
//        $routes = Routes::getRoutesByCity($params['city']);
//
//        $response->data = $routes;
//        $response->format = Response::FORMAT_JSON;
//    }
//
//    public function actionTitle()
//    {
//        $params = Yii::$app->request->get();
//        $response = Yii::$app->response;
//
//        $routes = Routes::getRoutesByTitle($params['title']);
//
//        $response->data = $routes;
//        $response->format = Response::FORMAT_JSON;
//    }
}
