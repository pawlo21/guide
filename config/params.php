<?php

use yii\helpers\Url;

return [
    'google_api_key' => 'AIzaSyDbFNzFVxJ6fuoESN53Zjc9E_C9-u6NI50',
    'adminEmail' => 'admin@example.com',
    'media_paths' => [
        'city_logos' => 'uploads/cities',
        'point_imgs' => 'uploads/points/imgs',
        'point_thumb_imgs' => 'uploads/points/imgs/thumb',
        'point_sounds' => 'uploads/points/mp3',
    ],
];
