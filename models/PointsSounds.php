<?php

namespace app\models;

use Yii;
use yii\helpers\Url;

/**
 * This is the model class for table "points_sounds".
 *
 * @property int $id
 * @property int $point_id
 * @property string $sound
 * @property string $title
 *
 * @property Points $point
 */
class PointsSounds extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'points_sounds';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['point_id'], 'integer'],
            [['sound'], 'file', 'extensions' => 'mp3', 'maxSize' => 1024 * 1024 * 10, 'maxFiles' => 0],
            [['point_id'], 'exist', 'skipOnError' => true, 'targetClass' => Points::className(), 'targetAttribute' => ['point_id' => 'id']],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'point_id' => 'Point ID',
            'title' => 'Sound title',
            'sound' => 'Sound',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPoint()
    {
        return $this->hasOne(Points::className(), ['id' => 'point_id']);
    }

    public static function getSoundsByPointId($point_id)
    {
        $sounds = PointsSounds::find()
            ->asArray()
            ->where(['point_id' => $point_id])
            ->all();

        $message = [];

        foreach ($sounds as $sound) {
            $message[] = [
                'id' => (int)$sound['id'],
                'sound_url' => Url::home(true) . Yii::$app->params['media_paths']['point_sounds'] . '/' . $sound['sound'],
                'short_url' => Yii::$app->params['media_paths']['point_sounds'] . '/' . $sound['sound'],
                'sound' => $sound['sound'],
                'title' => $sound['title'],
            ];
        }

        return $message;
    }
}
