<?php

namespace app\models;

use Yii;
use yii\helpers\Url;

/**
 * This is the model class for table "points_images".
 *
 * @property int $id
 * @property int $point_id
 * @property string $img
 * @property string $title
 *
 * @property Points $point
 */
class PointsImages extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'points_images';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['point_id'], 'integer'],
            [['img'], 'file', 'extensions' => 'gif, jpg, jpeg, svg, png', 'maxSize' => 1024 * 1024 * 10, 'maxFiles' => 0],
            [['point_id'], 'exist', 'skipOnError' => true, 'targetClass' => Points::className(), 'targetAttribute' => ['point_id' => 'id']],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'point_id' => 'Point ID',
            'title' => 'Image title',
            'img' => 'Img',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPoint()
    {
        return $this->hasOne(Points::className(), ['id' => 'point_id']);
    }

    public static function getImagesByPointId($point_id)
    {
        $images = PointsImages::find()
            ->asArray()
            ->where(['point_id' => $point_id])
            ->all();

        $message = [];

        foreach ($images as $image) {
            $message[] = [
                'id' => (int)$image['id'],
                'img_url' => Url::home(true) . Yii::$app->params['media_paths']['point_imgs'] . '/' . $image['img'],
                'thumb_img' => Url::home(true) . Yii::$app->params['media_paths']['point_thumb_imgs'] . '/' . $image['img'],
                'title' => $image['title'],
            ];
        }

        return $message;
    }
}
