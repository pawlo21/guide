<?php

namespace app\models;

use Yii;
use yii\helpers\Url;

/**
 * This is the model class for table "cities".
 *
 * @property int $id
 * @property string $title
 * @property string $logo
 *
 * @property Routes[] $routes
 */
class Cities extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cities';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'string', 'max' => 255],
            [['title'], 'required'],
            [['logo'], 'file', 'extensions' => 'gif, jpg, jpeg, svg, png', 'maxSize' => 1024 * 1024 * 10, 'tooBig' => 'File has to be smaller than 10MB']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'logo' => 'Logo',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoutes()
    {
        return $this->hasMany(Routes::className(), ['city_id' => 'id']);
    }

    public static function getCities()
    {
        $cities = Cities::find()
            ->asArray()
            ->all();

        $message = [];

        if ($cities) {
            foreach ($cities as $city) {
                $message[] = [
                    'id' => (int)$city['id'],
                    'title' => $city['title'],
                    'logo' => $city['logo'] !=null ? Url::home(true) . Yii::$app->params['media_paths']['city_logos'] . '/' . $city['logo'] : null,
                ];
            }
        }

        return $message;
    }

    public static function getCityById($city_id)
    {
        $city = Cities::find()
            ->where(['id' => $city_id])
            ->asArray()
            ->one();

        $message = [
            'id' => (int)$city['id'],
            'title' => $city['title'],
            'logo' => Url::home(true) . Yii::$app->params['media_paths']['city_logos'] . '/' . $city['logo']
        ];

        return $message;
    }
}
