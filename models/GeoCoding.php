<?php
/**
 * Created by PhpStorm.
 * User: developer
 * Date: 9/15/17
 * Time: 9:55 AM
 */

namespace app\models;

use yii\base\Model;
use yii\httpclient\Client;
use Yii;
use yii\helpers\VarDumper;

class GeoCoding extends Model
{
    public static function reverseGeoCoding($place)
    {
        $client = new Client();
        $response = $client->createRequest()
            ->setMethod('get')
            ->setUrl('https://maps.google.com/maps/api/geocode/json')
            ->setData(['key' => Yii::$app->params['google_api_key'], 'address' => $place])
            ->send();

        $data = $response->data;
        if ($data['status'] == 'OK') {
            $lat = $data['results'][0]['geometry']['location']['lat'];
            $lng = $data['results'][0]['geometry']['location']['lng'];
            return $coords = [
                'lat' => $lat,
                'lng' => $lng,
            ];
        } else {
            return null;
        }

    }

    public static function geoCoding($lat, $lng)
    {
        $client = new Client();
        $response = $client->createRequest()
            ->setMethod('get')
            ->setUrl('https://maps.google.com/maps/api/geocode/json')
            ->setData(['key' => Yii::$app->params['google_api_key'], 'latlng' => $lat . ',' . $lng])
            ->send();

        $data = $response->data;
        if ($data['status'] == 'OK') {
            $address = $data['results'][0]['formatted_address'];
            return $address;
        } else {
            return null;
        }

    }
}