<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "points".
 *
 * @property int $id
 * @property int $city_id
 * @property string $lat
 * @property string $lng
 * @property string $description
 * @property string $title
 * @property string $address
 *
 * @property Destinations[] $destinations
 * @property PointsImages[] $pointsImages
 */
class Points extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'points';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['lat', 'lng', 'description', 'title', 'address'], 'string', 'max' => 255],
            [['description'], 'string', 'max' => 1000],
            [['title', 'description', 'address', 'city_id'], 'required'],
            [['city_id'], 'integer'],
            [['city_id'], 'exist', 'skipOnError' => true, 'targetClass' => Cities::className(), 'targetAttribute' => ['city_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'city_id' => 'City',
            'lat' => 'Lat',
            'lng' => 'Lng',
            'description' => 'Description',
            'title' => 'Title',
            'address' => 'Address',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDestinations()
    {
        return $this->hasMany(Destinations::className(), ['point_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPointsImages()
    {
        return $this->hasMany(PointsImages::className(), ['point_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(Cities::className(), ['id' => 'city_id']);
    }

    public static function getPoints($city_id)
    {
        $points = Points::find()
            ->where(['city_id' => $city_id])
            ->asArray()
            ->all();

        $message = [];

        if ($points) {
            foreach ($points as $point) {
                $message[] = [
                    'id' => (int)$point['id'],
                    'title' => $point['title'],
                    'description' => $point['description'],
                    'city_id' => (int) $point['city_id'],
                    'lat' => (float)$point['lat'],
                    'lng' => (float)$point['lng'],
                    'address' => $point['address'],
                    'images' => PointsImages::getImagesByPointId($point['id']),
                    'sounds' => PointsSounds::getSoundsByPointId($point['id']),
                ];
            }
        }

        return $message;
    }

    public static function getPointsByRouteId($route_id)
    {
        $destinations = Destinations::find()
            ->asArray()
            ->where(['route_id' => $route_id])
            ->all();

        $message = [];

        foreach ($destinations as $destination) {
            $point = Points::findOne([$destination['point_id']]);
            $message[] = [
                'id' => (int)$point['id'],
                'title' => $point['title'],
                'description' => $point['description'],
                'city_id' => (int) $point['city_id'],
                'lat' => (float)$point['lat'],
                'lng' => (float)$point['lng'],
                'address' => $point['address'],
                'images' => PointsImages::getImagesByPointId($point['id']),
                'sounds' => PointsSounds::getSoundsByPointId($point['id']),
            ];
        }

        return $message;
    }

    public static function getPoint($point_id)
    {
        $point = Points::find()
            ->where(['id' => $point_id])
            ->asArray()
            ->one();

        $message = [];

        if ($point) {
            $message = [
                'id' => (int)$point['id'],
                'title' => $point['title'],
                'description' => $point['description'],
                'city_id' => (int) $point['city_id'],
                'lat' => (float)$point['lat'],
                'lng' => (float)$point['lng'],
                'address' => $point['address'],
                'images' => PointsImages::getImagesByPointId($point['id']),
                'sounds' => PointsSounds::getSoundsByPointId($point['id']),
            ];
        }

        return $message;
    }
}
