<?php

namespace app\models;

/**
 * This is the model class for table "routes".
 *
 * @property int $id
 * @property string $title
 * @property int $city_id
 *
 * @property Destinations[] $destinations
 * @property Cities $city
 */
class Routes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'routes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['city_id'], 'integer'],
            [['title'], 'string', 'max' => 255],
            [['title', 'city_id'], 'required'],
            [['city_id'], 'exist', 'skipOnError' => true, 'targetClass' => Cities::className(), 'targetAttribute' => ['city_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'city_id' => 'City',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDestinations()
    {
        return $this->hasMany(Destinations::className(), ['route_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(Cities::className(), ['id' => 'city_id']);
    }

    public static function getRoutes($city_id)
    {
        $routes = Routes::find()
            ->where(['city_id' => $city_id])
            ->asArray()
            ->all();

        $message = [];

        foreach ($routes as $route) {
            $message[] = [
                'id' => (int)$route['id'],
                'title' => $route['title'],
                'city_id' => (int)$city_id,
                'city' => Cities::findOne([$route['city_id']])->title,
                'points' => Points::getPointsByRouteId($route['id']),
            ];
        }

        return $message;
    }

    public static function getRouteById($route_id)
    {
        $route = Routes::find()
            ->where(['id' => $route_id])
            ->asArray()
            ->one();

        $message = [];

        if ($route) {
            $message = [
                'id' => (int)$route['id'],
                'title' => $route['title'],
                'city' => Cities::findOne([$route['city_id']])->title,
                'city_id' => (int)$route['city_id'],
                'points' => Points::getPointsByRouteId($route['id']),
            ];
        }

        return $message;
    }

    public static function getRouteByTitleAndCity($title, $city_id)
    {
        $route = Routes::find()
            ->where(['title' => $title])
            ->andWhere(['city_id' => $city_id])
            ->asArray()
            ->one();

        return $route;
    }
}
