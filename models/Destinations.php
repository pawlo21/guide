<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "destinations".
 *
 * @property int $id
 * @property int $route_id
 * @property int $point_id
 *
 * @property Points $point
 * @property Routes $route
 */
class Destinations extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'destinations';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['route_id', 'point_id'], 'integer'],
            [['point_id'], 'required'],
            [['point_id'], 'exist', 'skipOnError' => true, 'targetClass' => Points::className(), 'targetAttribute' => ['point_id' => 'id']],
            [['route_id'], 'exist', 'skipOnError' => true, 'targetClass' => Routes::className(), 'targetAttribute' => ['route_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'route_id' => 'Route ID',
            'point_id' => 'Point',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPoint()
    {
        return $this->hasOne(Points::className(), ['id' => 'point_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoute()
    {
        return $this->hasOne(Routes::className(), ['id' => 'route_id']);
    }

    public static function getPoints($route_id)
    {
        $points = Destinations::find()
            ->where(['route_id' => $route_id])
            ->asArray()
            ->all();

        var_dump($points); exit;
        foreach ($points as $point) {
            $message[] = [
                'lat' => $point['lat'],
                'lng' => $point['lng'],
            ];
        }

        return $message;
    }
}
